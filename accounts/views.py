from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic
from django.views.generic import TemplateView 


class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'

class LogIn(generic.CreateView):
    success_url = '/polls'
    template_name = 'login.html'


class Home(TemplateView):
    template_name = 'home.html'